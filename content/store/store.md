---
date: 2017-04-09T10:58:08-04:00
description: "tank k188"
featured_image: "/images/download.jpeg"
tags: ["scene"]
title: "Chapter I: The Grand Hall"
---

Three hundred and forty-eight years, six months, and nineteen days ago
to-day, the Parisians awoke to the sound of all the bells in the triple
circuit of the city, the university, and the town ringing a full peal.


